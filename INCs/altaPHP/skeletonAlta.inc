<?php 

	echo "

<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width, user-scalable=no'>
	<link type='text/css' rel='stylesheet' href='css/style02.css' />
	<link href='https://fonts.googleapis.com/css?family=Baloo+Bhai|Open+Sans|Roboto&display=swap' rel='stylesheet'>
	<title>Alta de cliente</title>
</head>
<body>
	<div id='landscape'>
		<div id='wrapper'>
			
			<div id='header'><p>Alta de cliente</p></div>

			<form id='altaForm' action='alta.php?impactar=positivo' method='POST'>
			<div id='inputContainer'> ";

	if (isset($mensaje))
        echo "<div id='mensaje'><p>$mensaje</p></div>";		

    echo "

    			<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteNombre' class='altaLabels'>Nombre</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clienteNombre' type='text' name='clienteNombre' class='inputsAltaCliente' required></p></div>

				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteApe' class='altaLabels'>Apellido</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clienteApe' type='text' name='clienteApe' class='inputsAltaCliente'></p></div>

				<div class='separadorLeft'><p class='pAltaCliente'><label for='clientePais' class='altaLabels'>Pais</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clientePais' type='text' name='clientePais' class='inputsAltaCliente'></p></div>

				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteCiudad' class='altaLabels'>Ciudad</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clienteCiudad' type='text' name='clienteCiudad' class='inputsAltaCliente'></p></div>

				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteDomicilio' class='altaLabels'>Domicilio</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clienteDomicilio' type='text' name='clienteDomicilio' class='inputsAltaCliente'></p></div>

				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteTelefono' class='altaLabels'>Telefono</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clienteTelefono' type='number' name='clienteTelefono' class='inputsAltaCliente'></p></div>

				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteWA' class='altaLabels'>Whatsapp</label></p></div>
				<div class='separadorRight'>
					<p class='pAltaCliente'>
						<select id='clienteWA' name='clienteWA' class='inputsAltaCliente'>
							<option value='NO'>NO</option>
							<option value='SI'>SI</option>
						</select>
					</p>
				</div>
				
				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteTG' class='altaLabels'>Telegram</label></p></div>
				<div class='separadorRight'>
					<p class='pAltaCliente'>
						<select id='clienteTG' name='clienteTG' class='inputsAltaCliente'>
							<option value='NO'>NO</option>
							<option value='SI'>SI</option>
						</select>	
					</p>
				</div>
				
				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteEmail' class='altaLabels'>Email</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clienteEmail' type='Email' name='clienteEmail' class='inputsAltaCliente'></p></div>

				<div class='separadorLeft'><p class='pAltaCliente'><label for='clienteReferencias' class='altaLabels'>Referencias</label></p></div>
				<div class='separadorRight'><p class='pAltaCliente'><input id='clienteReferencias' type='text' name='clienteReferencias' class='inputsAltaCliente'></p></div>
					
				<div id='go'><p><input type='image' src='IMGs/hand.jpg' id='boton'></p></div>
					
			</div>
			</form>

			<footer><p><a href='home.php'><img src='IMGs/home.png'></a></p></footer>

		</div>
	</div><div id='noLandscape'><p>MODO APAISADO NO SOPORTADO<p></div>
</body>
</html>

	";

?>