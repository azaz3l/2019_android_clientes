<?php 

	echo "

<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width, user-scalable=no'>
	<link type='text/css' rel='stylesheet' href='css/style00.css' />
	<link href='https://fonts.googleapis.com/css?family=Baloo+Bhai|Open+Sans|Roboto&display=swap' rel='stylesheet'>
	<title>Clientes</title>
</head>
<body>
	<div id='landscape'>
	<div id='wrapper'>

		<div id='header'><p>Ingreso</p></div>

		<div id='imgLogin'><img id='logologin' src='IMGs/login.jpg' alt='logologin'/></div>
		
		<div id='inputContainer'>
			<form id='loginForm' action='login.php?door=limon' method='POST'> ";


if (isset($mensaje))
	echo "<div id='mensaje'><p>$mensaje</p></div>";


echo "

				<div class='separadorLeft'><p class='ploginUC'><label for='miusuario' class='loginLabels'>Usuario</label></p></div>
				<div class='separadorRight'>
					<p class='ploginUC'>
						<input id='miusuario' type='text' name='miusuario' class='loginInputs' value='"; ObtenerValorDeInput('miusuario'); echo "' autofocus required>
					</p>										<!-- lo de arriba es para no olvidar el valor del input cuando la clave no valida correctamente -->
				</div>

				<div class='separadorLeft'><p class='ploginUC'><label for='mipass' class='loginLabels' >Clave</label></p></div>
				<div class='separadorRight'><p class='ploginUC'><input id='mipass' type='password' name='mipass' class='loginInputs' required></p></div>

				<div id='in'><p><input type='image' src='IMGs/hand.jpg' id='boton'/><p></div>

			</form>
		</div>

	</div>
	</div><div id='noLandscape'><p>MODO APAISADO NO SOPORTADO<p></div>
</body>
</html>				 

			";

?>