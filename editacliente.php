<?php 

session_start();
  if (@$_SESSION['auth'] != "si")
    {
	  header("Location: login.php");
	  exit();
    }

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<link type="text/css" rel="stylesheet" href="css/style04.css" />
	<link href="https://fonts.googleapis.com/css?family=Baloo+Bhai|Open+Sans|Roboto&display=swap" rel="stylesheet">
	<title>Editar</title>
</head>
<body>
	<div id='landscape'>
		<div id='wrapper'>
			
			<div id='header'><p>Editar cliente</p></div>

			<form id="traerCliente" action="editacliente.php?aidi=idclient" method="POST">
			<div class="containerEdit">
				
<?php

if (isset($mensaje))
        echo "<div id='mensaje'><p>$mensaje</p></div>";

?>

				<div class="separadorLCR separadorL"><p class="pEdit"><label for="enviaId" class="editLabels">Id</label></p></div>
				<div class="separadorLCR separadorC">
					<p class="pEdit"><input id="enviaId" type="text" name="enviaId" class="inputsEdit" required /></p>
				</div>
				<div class="separadorLCR separadorR"><p class="pEdit"><input type="image" src="IMGs/hand.jpg" id="boton"></p></div>	
			</div>	
			</form> 			

			<div class="containerEdit">
				<div class="divisor1234 divisor1"><p class="pEdit"><label class="showLabels" for="showId">Nombre</label></p></div>
				<div class="divisor1234 divisor2"><p class="pEdit"><input id="showId" class="inputShow" type="text" name="showId"></p></div>
				<div class="divisor1234 divisor3"><p class="pEdit"><label class="showLabels" for="showName">Apellido</label></p></div>
				<div class="divisor1234 divisor4">
					<p class="pEdit"><input id="showName" class="inputShow" type="text" name="showName"></p>
				</div>
				<br />
				<div class="divisor1234 divisor1"><p class="pEdit"><label class="showLabels" for="showPais">Pais</label></p></div>
				<div class="divisor1234 divisor2">
					<p class="pEdit"><input id="showPais" class="inputShow" type="text" name="showPais"></p>
				</div>
				<div class="divisor1234 divisor3"><p class="pEdit"><label class="showLabels" for="showCiudad">Ciudad</label></p></div>
				<div class="divisor1234 divisor4">
					<p class="pEdit"><input id="showCiudad" class="inputShow" type="text" name="showCiudad"></p>
				</div>
				<br />
				<div class="divisor1234 divisor1"><p class="pEdit"><label class="showLabels" for="showDomicilio">Domicilio</label></p></div>
				<div class="divisor1234 divisor2">
					<p class="pEdit"><input id="showDomicilio" class="inputShow" type="text" name="showDomicilio"></p>
				</div>
				<div class="divisor1234 divisor3"><p class="pEdit"><label class="showLabels" for="showTelefono">Telefono</label></p></div>
				<div class="divisor1234 divisor4">
					<p class="pEdit"><input id="showTelefono" class="inputShow" type="text" name="showTelefono"></p>
				</div>
				<br />
				<div class="divisor1234 divisor1"><p class="pEdit"><label class="showLabels" for="showWA">Whatsapp</label></p></div>
				<div class="divisor1234 divisor2">
					<p class="pEdit"><input id="showWA" class="inputShow" type="text" name="showWA"></p>
				</div>
				<div class="divisor1234 divisor3"><p class="pEdit"><label class="showLabels" for="showTelegram">Telegram</label></p></div>
				<div class="divisor1234 divisor4">
					<p class="pEdit"><input id="showTelegram" class="inputShow" type="text" name="showTelegram"></p>
				</div>
				<br />
				<div class="divisor1234 divisor1"><p class="pEdit"><label class="showLabels" for="showEmail">Email</label></p></div>
				<div class="divisor1234 divisor2">
					<p class="pEdit"><input id="showEmail" class="inputShow" type="text" name="showEmail"></p>
				</div>
				<div class="divisor1234 divisor3"><p class="pEdit"><label class="showLabels" for="showRef">Referencias</label></p></div>
				<div class="divisor1234 divisor4">
					<p class="pEdit"><input id="showRef" class="inputShow" type="text" name="showRef"></p>
				</div>
			</div>

			<footer><p><a href="home.php"><img src="IMGs/home.png"></a></p></footer>

		</div>
	</div><div id='noLandscape'><p>MODO APAISADO NO SOPORTADO<p></div>
</body>
</html>