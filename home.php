<?php 

session_start();
  if (@$_SESSION['auth'] != "si")
    {
	  header("Location: login.php");
	  exit();
    }

?>



<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<link type="text/css" rel="stylesheet" href="css/style01.css" />
	<link href="https://fonts.googleapis.com/css?family=Baloo+Bhai|Open+Sans|Roboto&display=swap" rel="stylesheet">
	<title>Menu</title>
</head>
<body>
	<div id="landscape">
		<div id="wrapper">
			
				<div id="header"><p>Menu</p></div>

				<div id="opciones">

					<div class="options">
						<div class="btnLabels"><p>Alta de cliente</p></div>
						<div class="BtnImg"><p><a href="alta.php"><input type="image" src="IMGs/hand.jpg" id="boton"/></a><p></div>
					</div>
					<div class="options">
						<div class="btnLabels"><p>Buscar cliente</p></div>
						<div class="BtnImg"><p><a href="buscacliente.php"><input type="image" src="IMGs/hand.jpg" id="boton"/></a></p></div>
					</div>
					<div class="options">
						<div class="btnLabels"><p>Editar cliente</p></div>
						<div class="BtnImg"><p><a href="editacliente.php"><input type="image" src="IMGs/hand.jpg" id="boton"/></a></p></div>
					</div>
					<div class="options">
						<div class="btnLabels"><p>Salir</p></div>
						<div class="BtnImg"><p><a href="exit.php"><input type="image" src="IMGs/hand.jpg" id="boton"/></a></p></div>	
					</div>
				</div>
		</div>
	</div><div id="noLandscape"><p>MODO APAISADO NO SOPORTADO<p></div>

</body>
</html>